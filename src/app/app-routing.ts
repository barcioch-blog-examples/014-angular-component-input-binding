import { Routes } from '@angular/router';
import { RoutedComponentComponent } from '../routed-component/routed-component.component';


export const routes: Routes = [
  {
    path: 'params/:pathParam',
    component: RoutedComponentComponent,
    data: {dataParam: 'secret'}
  },
];
