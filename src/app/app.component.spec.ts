import { provideRouter, withComponentInputBinding, withRouterConfig } from '@angular/router';
import { TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { routes } from './app-routing';
import { RouterTestingHarness } from '@angular/router/testing';
import { RoutedComponentComponent } from '../routed-component/routed-component.component';
import { NonRoutedComponentComponent } from '../non-routed-component/non-routed-component.component';


describe('ComponentInputBinding', () => {
  let harness: RouterTestingHarness;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [RoutedComponentComponent, NonRoutedComponentComponent],
      providers: [provideRouter(routes, withComponentInputBinding(), withRouterConfig({ paramsInheritanceStrategy: 'always' }))],
    });

    harness = await RouterTestingHarness.create();
  });

  describe('Test standard route params', () => {
    describe('when user enters /params/123?queryParam=paramFromQuery url', () => {
      beforeEach(async () => {
        await harness.navigateByUrl('/params/123?queryParam=paramFromQuery');
        harness.fixture.detectChanges();
      });

      it('should display routed component params', () => {
        const expected: ComponentParams = {
          pathParam: '123',
          queryParam: 'paramFromQuery',
          nonExistingParam: '',
          dataParam: 'secret',
        };
        expect(getRoutedComponentParams()).toEqual(expected);
      });

      it('should display empty input values in non-routed component', () => {
        const expected: ComponentParams = {
          pathParam: '',
          queryParam: '',
          nonExistingParam: '',
          dataParam: '',
        };
        expect(getNonRoutedComponentParams()).toEqual(expected);
      });

      describe('and user navigates to same route with different pathParam and queryParam (/params/456?queryParam=aNewValue)', () => {
        beforeEach(async () => {
          await harness.navigateByUrl('/params/456?queryParam=aNewValue');
          harness.fixture.detectChanges();
        });

        it('should display routed component params', () => {
          const expected: ComponentParams = {
            pathParam: '456',
            queryParam: 'aNewValue',
            nonExistingParam: '',
            dataParam: 'secret',
          };
          expect(getRoutedComponentParams()).toEqual(expected);
        });

        it('should display empty input values in non-routed component', () => {
          const expected: ComponentParams = {
            pathParam: '',
            queryParam: '',
            nonExistingParam: '',
            dataParam: '',
          };
          expect(getNonRoutedComponentParams()).toEqual(expected);
        });
      });
    });
  });

  describe('Test name collision', () => {
    describe('when user enters url with query params named same as pathParam and dataParam (/params/123?pathParam=pathParamFromQuery&dataParam=dataParamFromQuery)', () => {
      beforeEach(async () => {
        await harness.navigateByUrl('/params/123?pathParam=pathParamFromQuery&dataParam=dataParamFromQuery');
        harness.fixture.detectChanges();
      });

      it('should not override path params and data', () => {
        const expected: ComponentParams = {
          pathParam: '123',
          queryParam: '',
          nonExistingParam: '',
          dataParam: 'secret',
        };
        expect(getRoutedComponentParams()).toEqual(expected);
      });

      it('should display empty input values in non-routed component', () => {
        const expected: ComponentParams = {
          pathParam: '',
          queryParam: '',
          nonExistingParam: '',
          dataParam: '',
        };
        expect(getNonRoutedComponentParams()).toEqual(expected);
      });
    });
  });

  const getRoutedComponentParams = (): ComponentParams => {
    return {
      pathParam: harness.fixture.debugElement.query(By.css('[data-test="routed-pathParam"]')).nativeElement.textContent,
      queryParam: harness.fixture.debugElement.query(By.css('[data-test="routed-queryParam"]')).nativeElement.textContent,
      nonExistingParam: harness.fixture.debugElement.query(By.css('[data-test="routed-nonExistingParam"]')).nativeElement.textContent,
      dataParam: harness.fixture.debugElement.query(By.css('[data-test="routed-dataParam"]')).nativeElement.textContent,
    }
  }

  const getNonRoutedComponentParams = (): ComponentParams => {
    return {
      pathParam: harness.fixture.debugElement.query(By.css('[data-test="non-routed-pathParam"]')).nativeElement.textContent,
      queryParam: harness.fixture.debugElement.query(By.css('[data-test="non-routed-queryParam"]')).nativeElement.textContent,
      nonExistingParam: harness.fixture.debugElement.query(By.css('[data-test="non-routed-nonExistingParam"]')).nativeElement.textContent,
      dataParam: harness.fixture.debugElement.query(By.css('[data-test="non-routed-dataParam"]')).nativeElement.textContent,
    }
  }
});

interface ComponentParams {
  pathParam: string;
  queryParam: string;
  nonExistingParam: string;
  dataParam: string;
}
