import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { provideRouter, RouterOutlet, withComponentInputBinding, withRouterConfig } from '@angular/router';
import { routes } from './app-routing';
import { RoutedComponentComponent } from '../routed-component/routed-component.component';
import { NonRoutedComponentComponent } from '../non-routed-component/non-routed-component.component';

@NgModule({
  declarations: [
    AppComponent,
    RoutedComponentComponent,
    NonRoutedComponentComponent
  ],
  imports: [
    BrowserModule,
    RouterOutlet,
  ],
  providers: [provideRouter(routes, withComponentInputBinding(), withRouterConfig({paramsInheritanceStrategy: 'always'}))],
  bootstrap: [AppComponent]
})
export class AppModule {
}
