import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-non-routed-component',
  template: `
    <span data-test="non-routed-pathParam">{{ pathParam }}</span>
    <span data-test="non-routed-queryParam">{{ queryParam }}</span>
    <span data-test="non-routed-nonExistingParam">{{ nonExistingParam }}</span>
    <span data-test="non-routed-dataParam">{{ dataParam }}</span>
  `
})
export class NonRoutedComponentComponent {
  @Input() pathParam?: string;
  @Input() nonExistingParam?: string;
  @Input() queryParam?: string;
  @Input() dataParam?: string;
}
