import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-routed-component',
  template: `
    <span data-test="routed-pathParam">{{ pathParam }}</span>
    <span data-test="routed-queryParam">{{ queryParam }}</span>
    <span data-test="routed-nonExistingParam">{{ nonExistingParam }}</span>
    <span data-test="routed-dataParam">{{ dataParam }}</span>
    
    <app-non-routed-component></app-non-routed-component>
  `
})
export class RoutedComponentComponent {
  @Input() pathParam?: string;
  @Input() nonExistingParam?: string;
  @Input() queryParam?: string;
  @Input() dataParam?: string;
}
